import React, { Component } from "react";
import fire from "./firebase.js";
import "./App.css";

let artworkUrlBase = "https://fillyradio.com/cover/500";

class PrevPlayed extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return <React.Fragment />;
  }
}

class DescriptionPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nowPlaying: [],
      artworkUrl:
        "https://fillyradio.com/cover/500?nocache=" + new Date().getTime()
    };
  }
  componentWillMount() {
    let nowPlayingData = fire
      .database()
      .ref("rest/fillydelphia-radio/now-playing");

    nowPlayingData.on("value", snapshot => {
      let playingData = snapshot.val();
      this.setState({
        nowPlaying: playingData,
        artworkUrl: artworkUrlBase + "?nocache=" + new Date().getTime()
      });
    });
  }
  render() {
    return (
      <React.Fragment>
        <div className="art">
          <img src={this.state.artworkUrl} alt="Cover Art" />
        </div>
        <div className="desc">
          <span>
            {this.state.nowPlaying.title
              ? this.state.nowPlaying.title
              : "Loading"}
          </span>
        </div>
      </React.Fragment>
    );
  }
}

const HomeScreen = () => (
  <React.Fragment>
    <DescriptionPanel />
    {/* <PrevPlayed /> */}
    {/* <MobileControls */}
  </React.Fragment>
);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {}
  render() {
    return (
      <div className="App">
        <div className="container">
          {/* <Logo /> */}
          {/* <Nav /> */}
          <HomeScreen />
        </div>
      </div>
    );
  }
}

export default App;
