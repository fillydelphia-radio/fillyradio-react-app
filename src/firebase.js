import * as firebase from "firebase";

var config = {
  authDomain: "fillydelphia-radio.firebaseapp.com",
  databaseURL: "https://fillydelphia-radio.firebaseio.com",
  projectId: "fillydelphia-radio",
  storageBucket: "fillydelphia-radio.appspot.com",
  messagingSenderId: "404657910771"
};
var fire = firebase.initializeApp(config);
export default fire;
